import React from "react";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import '../scss/font.scss';
import '../scss/resets.scss';
import '../scss/App.scss';
import HomePage from '../components/HomePage';
import Contactus from '../components/contactus';
import Aboutus from '../components/aboutus';
import Blog from '../components/BlogScreen';
import Blogs from '../components/Blog';
import OurServices from '../components/OurServices';
import LandR from '../components/LoveAndRelation'
import Product from '../components/Product';
import MainProduct from '../components/MainProduct';

function App() {
  return (
   
    <Router>
      {/* <Header /> */}
<div className="app">
      <Switch>
        <Route exact path='/' component={HomePage} />
        <Route path='/contact' component={Contactus} />
        <Route path="/aboutus" component={Aboutus}/>
         <Route path="/blog" component={Blog}/>
          <Route path="/blogs" component={Blogs}/>
          <Route path="/services" component={OurServices}/>
          <Route path="/loveandrelationship" component={LandR}/>
          <Route path="/product" component={Product}/>
           <Route path="/mainproduct" component={MainProduct}/>

      </Switch>
      </div>

</Router> 

      // <Contactus location={location} zoomLevel={17}/>
    // </div>
  );
}

export default App;
